# firth

_Salad Fingers Character Mod for [Don't Starve](https://www.klei.com/games/dont-starve)._

![Salad Fingers](firth.png)

## Installation

```
git clone https://gitlab.com/hexgear/firth.git \
  ~/.local/share/Steam/steamapps/common/dont_starve/mods/firth
```

## Compiling

```
docker-compose run ds_mod_tools
/opt/ds_mod_tools/scml exported/firth/firth.scml .
```

## Acknowledgement

[Salad Fingers](https://en.wikipedia.org/wiki/Salad_Fingers) is the creative work of [David Firth](http://www.fat-pie.com).

Don't Starve is the property of [Klei Entertainment](https://www.kleientertainment.com).

## License

SPDX-License-Identifier: [CC-BY-3.0](https://creativecommons.org/licenses/by/3.0)

## Reference

- [Getting Started:  Guides, Tutorials And Examples - Tutorials & Guides - Klei Entertainment Forums](https://forums.kleientertainment.com/forums/topic/28021-getting-started-guides-tutorials-and-examples)

## See Also

- [Extended Sample Character for Don't Starve Together](https://github.com/DragonWolfLeo/extendedsamplecharacter-dontstarvetogether)

